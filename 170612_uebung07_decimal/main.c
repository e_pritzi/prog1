#include <stdio.h>
#include <stdlib.h>
#include "decimal.h"



int main()
{
    Number_t numberArray[MAXARRAY];

    long choice;
    //Loop starten
    do
    {
        //Menue ausgeben
        printf("\nBitte wahlen Sie einen Punkt aus: \n");
        printf("1 - Initalize all\n");
        printf("2 - Edit Number\n");
        printf("3 - Print Number\n");
        printf("4 - Print all Numbers\n");
        printf("5 - Write to file\n");
        printf("6 - Read from file\n");
        printf("7 - Set to zero\n");
        printf("8 - COMP\n");
        printf("9 - Ende\n");

        scanf("%ld", &choice);

        long whichNumber, whichNumber2;
        //Wahl untersuchen
        switch(choice)
        {
            case 1:
                //Initialize all
                initializeAll(numberArray, MAXARRAY);
                break;

            case 2:
                //Edit Number

                printf("Welche Nummer moechten Sie aendern? (Index von 0): ");
                scanf("%ld", &whichNumber);
                editNumber(&numberArray[whichNumber]);
                break;

            case 3:
                //eine Nummer ausgeben
                printf("Welche Nummer moechten Sie ausgeben? (Index von 0): ");
                scanf("%ld", &whichNumber);
                printNumber(&numberArray[whichNumber]);
                break;

            case 4:
                //alle ausgeben
                printNumbers(numberArray, MAXARRAY);
                break;

            case 5:
                //in Datei schreiben
                writeNumbers(numberArray, MAXARRAY, "test");
                break;

            case 6:
                //von Datei lesen
                readNumbers(numberArray, MAXARRAY, "test");
                break;

            case 7:
                //eine Nummer ausgeben
                printf("Welche Nummer moechten Sie zuruecksetzen? (Index von 0): ");
                scanf("%ld", &whichNumber);
                setZero(&numberArray[whichNumber]);
                break;

            case 8:
                printf("Welche Nummer moechten Sie vergleichen? (Index von 0): ");
                scanf("%ld", &whichNumber);
                printf("Welche Nummer moechten Sie vergleichen? (Index von 0): ");
                scanf("%ld", &whichNumber2);
                printf("%ld", compareNumbers(numberArray, MAXARRAY, whichNumber, whichNumber2));
                break;

            default:
                break;
        }


    }while(choice != 9);


    return 0;
}
