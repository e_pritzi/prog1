#include <stdio.h>
#include "decimal.h"

void initializeAll(Number_t st[], long len)
{
    long i;
    for(i = 0; i < len; i++)
    {
        long j;
        for(j = 0; j < MAXDIGIT; j++)
        {
            st[i].digits[j] = 0;
        }

        st[i].fpPosition = 0;
        st[i].sign = POSITIVE;
    }
}

void editNumber(Number_t *number)
{
    char choice;
    long lchoice;

    //clear buffer
    while(getchar() != '\n');

    printf("Bitte geben Sie das Vorzeichen ein: (+/-)");
    scanf("%c", &choice);
    if(choice == '+')
        number->sign = POSITIVE;
    else if(choice == '-')
        number->sign = NEGATIVE;

    printf("Bitte geben Sie die Position des Dezimalpunktes ein: ");
    scanf("%ld", &lchoice);
    number->fpPosition = lchoice;

    long index;
    do
    {
        printf("Bitte geben Sie den Index der zu aendernden Zahl ein (0 fuer Ende): ");
        scanf("%ld", &index);

        //auf Ende pruefen
        if(index == 0)
            break;

        index--;
        printf("Bitte geben Sie den neuen Wert der Zahl ein: ");
        scanf("%ld", &lchoice);
        number->digits[index] = lchoice;

    }while(1);

}

void printNumber(Number_t *number)
{
    printf("%c", ( number->sign == POSITIVE) ? '+' : '-');
    long i;
    for(i = MAXDIGIT - 1; i >= 0; i--)
    {
        printf("%ld", number->digits[i]);

        if(i == number->fpPosition)
        {
            //Kommapunkt ausgeben
            printf(".");
        }

    }
    printf("\n");
}

void printNumbers(Number_t number[], long len)
{
    long i = 0;

    for(i = 0; i < len; i++)
    {
        printNumber(&number[i]);
    }

}

void writeNumbers(Number_t numberArray[], long len, char filename[])
{
    FILE *myFile;

    if( (myFile = fopen(filename, "w+") ) == NULL)
    {
        //Fehler beim oeffnen der Datei
        fprintf(stderr, "Fehler beim Oeffnen der Datei!\n\n");
        return;
    }

    fwrite(numberArray, sizeof(Number_t), len, myFile);
    fclose(myFile);
}


void readNumbers(Number_t numberArray[], long len, char filename[])
{
    FILE *myFile;

    if( (myFile = fopen(filename, "r") ) == NULL)
    {
        //Fehler beim oeffnen der Datei
        fprintf(stderr, "Fehler beim Oeffnen der Datei!\n\n");
        return;
    }

    fread(numberArray, sizeof(Number_t), len, myFile);
    fclose(myFile);
}


void setZero(Number_t *number)
{
    long i;
    for(i = number->fpPosition - 1; i >= 0; i--)
    {
        number->digits[i] = 0;
    }
}

long compareNumbers(Number_t numberArray[], long len, long position1, long position2)
{
    long i = numberArray[position1].fpPosition, j = numberArray[position2].fpPosition;
    long cmp = 0;

    if(numberArray[position1].sign != numberArray[position2].sign)
    {
        return (numberArray[position1].sign == POSITIVE) ? -1 : 1;
    }


    while(i < len && j < len)
    {
        if(numberArray[position1].digits[i] < numberArray[position2].digits[j])
        {
            //2te groesser
            cmp = 1;
        }
        else if(numberArray[position1].digits[i] > numberArray[position2].digits[j])
        {
            cmp = -1;
        }
        i++;
        j++;
    }

    //falls eines laenger ist als das andere
    if(i > j)
    {
        //j hat noch stellen
        while(j < len)
        {
            if(numberArray[position2].digits[j] != 0)
            {
                cmp = 1;
                break;
            }
            j++;
        }
    }
    else if(j > i)
    {
        //i hat noch stellen
        while(i < len)
        {
            if(numberArray[position2].digits[i] != 0)
            {
                cmp = -1;
                break;
            }

            i++;
        }
    }

    if(cmp != 0)
    {
        //dann ist der groessere schon gefunden
        return cmp;
    }
    else
    {
        //ansonsten werden die nachkommastellen verglichen

        i = numberArray[position1].fpPosition - 1;
        j = numberArray[position2].fpPosition - 1;
        while(i >= 0 && j >= 0)
        {
            if(numberArray[position1].digits[i] < numberArray[position2].digits[j])
            {
                //2te groesser
                cmp = 1;
                break;
            }
            else if(numberArray[position1].digits[i] > numberArray[position2].digits[j])
            {
                cmp = -1;
                break;
            }
            i--;
            j--;
        }


        //jetzt bleibt nur noch die Moeglichkeit dass eines laenger ist als das andere
        if(i < j)
        {
            //j hat noch stellen
            while(j >= 0)
            {
                if(numberArray[position2].digits[j] != 0)
                {
                    cmp = 1;
                    break;
                }
                j--;
            }
        }
        else if(j < i)
        {
            //i hat noch stellen
            while(i >= 0)
            {
                if(numberArray[position2].digits[i] != 0)
                {
                    cmp = -1;
                    break;
                }
                i--;
            }
        }

    }

    if(numberArray[position1].sign == NEGATIVE)
    {
        //dann cmp drehen
        cmp = -cmp;
    }

    return cmp;
}
