#ifndef DECIMAL_H_INCLUDED
#define DECIMAL_H_INCLUDED

enum Signum_e
{
    POSITIVE,
    NEGATIVE
};

#define MAXDIGIT 50

typedef struct
{
    enum Signum_e sign;
    long digits[MAXDIGIT];
    long fpPosition;

}Number_t;

#define MAXARRAY 10

//Funktionen

void initializeAll(Number_t st[], long len);
void editNumber(Number_t *number);
void printNumber(Number_t *number);
void printNumbers(Number_t number[], long len);
void writeNumbers(Number_t numberArray[], long len, char filename[]);
void readNumbers(Number_t numberArray[], long len, char filename[]);
void setZero(Number_t *number);
long compareNumbers(Number_t numberArray[], long len, long position1, long position2);

#endif // DECIMAL_H_INCLUDED
