#include <stdio.h>
#include <string.h>

//Funktionsdeklarationen
void search(char filename[], char c);


int main(int argc, char *argv[]) //anzahl der elemente und strings argv[0] --> path
{
	char path[30];
	char c;
	if (argc > 2)
		c = argv[1]; //erstes argument nach dem Pfad gibt das Zeichen an
	else
		return 0;


	/*
	printf("Bitte geben Sie den gewuenschten Pfad ein: ");
	scanf("%s", path);
	while (getchar() != '\n');

	printf("\nGeben Sie ein nach welchem Zeichen gesucht werden soll: ");
	scanf("%c", &c);
	while (getchar() != '\n');
	*/


	long i = 2; //ab dem 2ten String beginnen die Pfade

	//jeden Pfag durchschauen
	while (i < argc)
	{		
		search(argv[i], c);
		i++;
	}
	getchar();
	return 0;
}

//sucht in einer Datei nach einem bestimmten Zeichen
void search(char filename[], char c)
{
	//man braucht einen Filepointer
	FILE * myFile;
	char cTemp;
	long n = 1; //Zeilennummer

	//oeffnen
	if ((myFile = fopen(filename, "r+")) == NULL)
	{
		//Datei kann nicht geoeffnet werden
		fprintf(stderr, "\n\nDie Datei \"%s\" kann nicht geoefnet werden!!\n", filename);
		return;
	}


	while ((cTemp = fgetc(myFile) ) != EOF) // bei Fehler oder Dateiende
	{
		if (cTemp == c)
		{
			//gesuchten Buchstaben gefunden
			printf("\n%s:\t", filename);
			printf("%ld", n);
		}
		else if (cTemp == '\n')
		{
			//Zeilennummer erhoehen
			n++;
		}
	}

	fclose(myFile);

}