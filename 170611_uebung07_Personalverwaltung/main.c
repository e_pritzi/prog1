#include <stdio.h>
#include <stdlib.h>
#include "personal.h"

#define NumWorkers 5

int main()
{
    //meine "Datenbank"
    Person_t workers[NumWorkers];
    char name[30];


    //Startwerte geben
    initializeAll(workers, NumWorkers);

    long choice;

    do
    {
        printf("Bitte waehlen Sie einen Punkt aus: \n");
        printf("1 - Eine Person ausgeben!\n");
        printf("2 - Alle Personen ausgeben!\n");
        printf("3 - Eine Person aendern!\n");
        printf("4 - In eine Datei schreiben!\n");
        printf("5 - Von einer Datei lesen!\n");
        printf("6 - Nach Gehalt sortieren!\n");
        printf("7 - Nach Abteilung sortieren!\n");
        printf("8 - Ende!\n\n");

        scanf("%ld", &choice);
        while(getchar() != '\n');

        cleanItems(workers, NumWorkers);

        switch(choice)
        {
            case 1:
                //1 Person ausgeben
                printf("Geben Sie den Index der Person ein: ");
                long index;
                scanf("%ld", &index);
                printPerson(workers[index]);
                break;

            case 2:
                printAllPersons(workers, NumWorkers);
                break;

            case 3:
                printf("Bitte geben Sie den Name der Person ein: ");
                scanf("%[^\n]s", name);
                editPerson(workers, NumWorkers, name);
                break;

            case 4:
                //in Datei schreiben
                printf("Bitte geben Sie den gewuenschten Dateipfad ein: ");
                scanf("%s", name);
                writeBinaryPersonData(workers, NumWorkers, name);
                break;

            case 5:
                //von Datei lesen
                printf("Bitte geben Sie den gewuenschten Dateipfad ein: ");
                scanf("%s", name);
                readBinaryPersonData(workers, NumWorkers, name);
                break;

            case 6:
                sortPersonData(workers, NumWorkers, &compareGehalt);
                break;

            case 7:
                sortPersonData(workers, NumWorkers, &compareAbteilung);
                break;

            default:
                break;
        }

    }while(choice != 8);

    return 0;
}
