#include "personal.h"
#include <string.h>
#include <stdio.h>

void initalizePerson(Person_t *person)
{
    person->abteilung = InitAbteilung;
    strcpy(person->name, InitName);
    person->telNr = InitTelNr;

    person->gehalt.betrag = InitBetrag;
    person->gehalt.gehaltsstufe = Initgehaltsstufe;
    person->gehalt.jahr = InitJahr;
    person->gehalt.monat = InitMonat;
}

void initializeAll(Person_t person[], long len)
{
    long i;
    for(i = 0; i < len; i++)
    {
        initalizePerson(&person[i]);
    }
}


//Ausgabe eines Datensatzes
void printPerson(Person_t person)
{
    printf("Name: \t%s\n", person.name);
    printf("Abteilung: \t%ld\n", person.abteilung);
    printf("Telefon: \t%ld\n", person.telNr);
    printf("Gehalt: \t%lf\n", person.gehalt.betrag);
    printf("Gehaltsstufe: \t%ld\n", person.gehalt.gehaltsstufe);
    printf("seit: \t%ld/%ld\n\n", person.gehalt.jahr, person.gehalt.monat);
}

void printAllPersons(Person_t person[], long len)
{
    long i;
    for(i = 0; i < len; i++)
    {
        printPerson(person[i]);
    }
}

void editPerson(Person_t person[], long len, char name[])
{
    long i;
    for(i = 0; i < len; i++)
    {
        if(! strcmp(person[i].name, name) )
        {
            char change;

            //Eintrag gefunden
            printf("Eintrag gefunden!\n");

            printf("Name: \t%s\n", person[i].name);
            printf("Wollen Sie den Eintrag aendern? (y/n)");

            while(getchar() != '\n');
            scanf("%c", &change);
            if(change == 'y')
            {
                printf("Bitte geben Sie den neuen Namen ein: ");
                scanf("%s", person[i].name);
            }

            printf("Abteilung: \t%ld\n", person[i].abteilung);
            printf("Wollen Sie den Eintrag aendern? (y/n)");

            while(getchar() != '\n');
            scanf("%c", &change);
            if(change == 'y')
            {
                printf("Bitte geben Sie die neue Abteilung ein: ");
                scanf("%ld", &person[i].abteilung);
            }

            printf("Telefon: \t%ld\n", person[i].telNr);
            printf("Wollen Sie den Eintrag aendern? (y/n)");

            while(getchar() != '\n');
            scanf("%c", &change);
            if(change == 'y')
            {
                printf("Bitte geben Sie die neue Telefonnummer ein: ");
                scanf("%ld", &person[i].telNr);
            }

            printf("Gehalt: \t%lf\n", person[i].gehalt.betrag);
            printf("Wollen Sie den Eintrag aendern? (y/n)");

            while(getchar() != '\n');
            scanf("%c", &change);
            if(change == 'y')
            {
                printf("Bitte geben Sie den neuen Betrag ein: ");
                scanf("%lf", &person[i].gehalt.betrag);
            }

            printf("Gehaltsstufe: \t%ld\n", person[i].gehalt.gehaltsstufe);
            printf("Wollen Sie den Eintrag aendern? (y/n)");

            while(getchar() != '\n');
            scanf("%c", &change);
            if(change == 'y')
            {
                printf("Bitte geben Sie die neue Gehaltsstufe ein: ");
                scanf("%ld", &person[i].gehalt.gehaltsstufe);
            }

            printf("seit: \t%ld/%ld\n\n", person[i].gehalt.jahr, person[i].gehalt.monat);
            printf("Wollen Sie den Eintrag aendern? (y/n)");

            while(getchar() != '\n');
            scanf("%c", &change);
            if(change == 'y')
            {
                printf("Bitte geben Sie das neue Jahr ein: ");
                scanf("%ld", &person[i].gehalt.jahr);
                printf("Bitte geben Sie den neuen Monat ein: ");
                scanf("%ld", &person[i].gehalt.monat);
            }

            break;
        }
    }

    if(i == len)
    {
        //Kein Eintrag gefunden
        printf("Die gewuenschte Person wurde nicht gefunden\n");

        for(i = 0; i < len; i++)
        {
            //auf freie Eintraege pruefen
            if(!strcmp(person[i].name, "") || person[i].abteilung < 0 || person[i].telNr < 0)
            {
                //ungueltiger Eintrag
                initalizePerson(&person[i]);
                break;
            }
        }

        if(i == len)
        {
            printf("Leider ist auch kein freier Eintrag verfuegbar!\n");
        }
    }
}


void writeBinaryPersonData(Person_t person[], long len, char *filename)
{
    FILE *myFile;
    myFile = fopen(filename, "w+");
    if(myFile != NULL)
    {
        //erfolgreich geoeffnet
        fwrite(person, sizeof(person[0]), len, myFile);
        fclose(myFile);
    }
    else
    {
        fprintf(stderr, "Fehler beim oeffnen der Datei!");
    }
}

void readBinaryPersonData(Person_t person[], long len, char *filename)
{
    FILE *myFile;
    myFile = fopen(filename, "r+");
    if(myFile != NULL)
    {
        //erfolgreich geoeffnet
        fread(person, sizeof(person[0]), len, myFile);
        fclose(myFile);
    }
    else
    {
        fprintf(stderr, "Fehler beim oeffnen der Datei!");
    }
}

void cleanItems(Person_t person[], long len)
{
    long i, correctEntry = 0;
    for(i = 0; i < len; i++)
    {
        if(strcmp(person[i].name, "") && person[i].abteilung > 0 && person[i].telNr > 0)
        {
            //Eintrag gueltig --> nach vorne schieben
            if(i > correctEntry)
            {
                //dann Positionstausch
                Person_t temp = person[i];
                person[i] = person[correctEntry];
                person[correctEntry] = temp;

            }
            correctEntry++;

        }
    }
}

/*
* return:   1 falls der Gehalt von a groe�er b
            0 falls der Gehalt von b groe�er a
*/
long compareGehalt(Person_t *a, Person_t *b)
{
    if(a->gehalt.betrag > b->gehalt.betrag)
        return 1;
    else
        return 0;
}


/*
* return:   1 falls der Gehalt von a groe�er b
            0 falls der Gehalt von b groe�er a
*/
long compareAbteilung(Person_t *a, Person_t *b)
{
    if(a->abteilung > b->abteilung)
        return 1;
    else
        return 0;
}

void sortPersonData(Person_t person[], long len, long (*compare) (Person_t *a, Person_t *b))
{
    long i, j;
    for(i = len-1; i > 0; i--)
    {
        //zuerst das ganze Feld durchgehen... danach steht das groe�te ganz hinten als nur noch bis n-1
        //solange bis i = 0
        for(j = 0; j < i; j++)
        {
            if((*compare) (&person[j], &person[j+1]))
            {
                // dann vertauschen
                Person_t temp = person[j];
                person[j] = person[j+1];
                person[j+1] = temp;
            }
        }
    }

}
