#ifndef PERSONAL_H_INCLUDED
#define PERSONAL_H_INCLUDED

//Persoenliche defines
#define NMAX 30
#define InitAbteilung 1
#define InitTelNr 1234567
#define InitName "Max Mustermann"
#define InitBetrag 99999
#define Initgehaltsstufe 1
#define InitJahr 1900
#define InitMonat 1

//Structdeklarationen
typedef struct Gehalt_s
{
    double betrag;
    long gehaltsstufe;
    long jahr;  //Datum an dem aktuelle Gehaltsstufe erreicht wurde
    long monat;

}Gehalt_t;

typedef struct Person_s
{
    char name[40]; //Vor und Nachname
    long abteilung; //Abteilungskennzahl
    long telNr;
    Gehalt_t gehalt;

}Person_t;


//Funktionsdeklarationen
void initalizePerson(Person_t *person);
void initializeAll(Person_t person[], long len);
void printPerson(Person_t person);
void printAllPersons(Person_t person[], long len);
void editPerson(Person_t person[], long len, char name[]);
void writeBinaryPersonData(Person_t person[], long len, char *filename);
void readBinaryPersonData(Person_t person[], long len, char *filename);
void cleanItems(Person_t person[], long len);
long compareGehalt(Person_t *a, Person_t *b);
long compareAbteilung(Person_t *a, Person_t *b);
void sortPersonData(Person_t person[], long len, long (*compare)(Person_t *a, Person_t *b));

#endif // PERSONAL_H_INCLUDED
