#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAXSTUDENTS 10
#define MAXEXAMS 5


//Structdeklarationen
typedef struct Exam_s
{
    char topic[100]; //Name der Lehrveranstaltung
    double points;
    double maxPoints;
    long mark; //Note

} Exam_t;

typedef struct Student_s
{
    char firstname[30];
    char lastname[30];
    long id;
    Exam_t exam; //Note

}Student_t;

//Funktionsdeklarationen
void initializeStudent(Student_t *student);
void initalizeAll(Student_t student[], long len);
void printExam(Exam_t exam);
void printStudent(Student_t student);
void printAllStudents(Student_t students[], long len);

int main()
{
    return 0;
}

//initialisiert einen Eintrag
void initializeStudent(Student_t *student)
{
    //jede id sollte nur einmal vergeben werden
    static long id = 0;

    strcpy(student->firstname, "Testname");
    strcpy(student->lastname, "Testname");
    student->id = id++;
    student->exam.mark = 0;
    student->exam.maxPoints = 100;
    student->exam.points = 0;
    strcpy(student->exam.topic, "Test");
}

void initalizeAll(Student_t student[], long len)
{
    long i;
    for(i = 0; i < len; i++)
    {
        initializeStudent(&student[i]);
    }
}

void printExam(Exam_t exam)
{
    fprintf(stdout, "LVA: \t%s\n", exam.topic);
    fprintf(stdout, "Punkte: \t%lf/%lf\n", exam.points, exam.maxPoints);
    fprintf(stdout, "Note: \t%ld\n\n", exam.mark);
}

void printStudent(Student_t student)
{
    fprintf(stdout, "Name: \t%s %s\n", student.firstname, student.lastname);
    fprintf(stdout, "Matrikelnummer: \t%7ld\n", student.id);
    fprintf(stdout, "Exam: \n");
    printExam(student.exam);
}

void printAllStudents(Student_t student[], long len)
{
    long i;
    for(i = 0; i < len; i++)
    {
        printStudent(student[i]);
    }
}
