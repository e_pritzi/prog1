#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define STRINGLEN 5

//Funktionsdeklarationen
void getline(char str[], long num);
char doreplace(char character, char search[], char replace[]);

//HAUPTPROGRAMM
int main()
{
    char search[STRINGLEN], replace[STRINGLEN], text[100];
    char *ptr = text;

    printf("Bitte geben Sie den ersten String ein: ");
    getline(search, STRINGLEN);

    printf("Bitte geben Sie den zweiten String ein: ");
    getline(replace, STRINGLEN);

    while(strlen(search) != strlen(replace))
    {
        printf("Die Strings sind ungleich lang. Bitte wiederholen Sie Ihre Eingabe: ");
        getline(replace, STRINGLEN);
    }

    printf("Bitte geben Sie den Text ein, der ausgetauscht werden soll: ");
    getline(text, 100);

    //Jetzt werden die Zeichen ausgetauscht
    while(*ptr != NULL)
    {
        *ptr = doreplace(*ptr, search, rep
                         lace);
        ptr++;
    }

    //und der fertige Text ausgegeben
    printf(text);

    return 0;
}

//string einlesen
void getline(char str[], long num)
{
    long i = 0;
    do
    {
        //Bereichsgrenze überschritten --> neu starten
        if(i >= num)
        {
           i = 0;
           printf("\nZeichenkette ist zu lang. Bitte nochmals eingeben! ");

           //restlichen Buffer löschen
           while(getchar() != '\n');
        }

        //Zeichen einlesen
        str[i++] = getchar();

    }while(str[i-1] != '\n');

    //neue Zeile durch String End Character ersetzen
    str[i-1] = '\0';
}

//Tauscht eine Variable von search aus
char doreplace(char character, char search[], char replace[])
{
    //liefert Pointer an die gewünschte Stelle
    char *ptr = strchr(search, character);

    //Austauschen --> *ptr-search = offset
    if(ptr != NULL)
        return replace[ptr - search];
    else
        return character;
}
