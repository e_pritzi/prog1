#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//struct - Deklarationen
struct Inventar_s
{
    char name[30];
    char nummer[6];
    long jahr;
};

//Funktionsdeklarationen
void printItem(struct Inventar_s *item);
void printList(struct Inventar_s items[], long len);
void insertItem(struct Inventar_s items[], long len);
void sortItems(struct Inventar_s items[], long len, int which);


//HAUPT
int main()
{
    struct Inventar_s InventarList[10]=
        {
        {"Name5", "1", 1997},
        {"Name4", "2", 1997},
        {"Name3", "3", 1997},
        {"Name4", "4", 2008},
        {"Name5", "5", 1997},
        {"Name5", "5", 1997},
        {"Name5", "5", 1997},
        {"Name5", "7", 1997},
        {"Name5", "5", 1997},
        {"Name5", "5", 1997}
        };

    sortItems(InventarList, 10, 2);

    long input;
    do
    {
        printf("-1- Alle Daten ausgeben\n");
        printf("-2- Eintrag hinzufuegen\n");
        printf("-3- Ende\n");
        scanf("%ld", &input);

        switch(input)
        {
            case 1:
                printList(InventarList, 10);
                break;

            case 2:
                insertItem(InventarList, 10);
                break;

            case 3:
                break;

            default:
                printf("\n\nUngueltige Eingabe!\n\n");
                break;
        }

    }while(input != 3);


    return 0;
}


//Funktionen
void printItem(struct Inventar_s *item)
{
    printf("Name: %s\n", (*item).name);
    printf("Nummer: %s\n", (*item).nummer);
    printf("Jahr: %ld\n\n", (*item).jahr);
}

//Liste ausgeben
void printList(struct Inventar_s items[], long len)
{
    long pos = 0;
    for(pos = 0; pos < len; pos++)
    {
        //ungueltige Eintraege filtern
        if(items[pos].nummer[0] != '\0')
            printItem(&items[pos]);
    }
}


//Eintrag einfügen
void insertItem(struct Inventar_s items[], long len)
{
    long index = 0;
    while(index < len)
    {
        if(items[index].nummer[0] == '\0')
            break;

        index++;
    }

    if(index >= len)
    {
        printf("\nKein freier Eintrag verfuegbar!\n");
        return;
    }

    char input[100] = "";
    do
    {
        printf("\nBitte geben Sie einen Namen fuer den Eintrag ein: ");
        scanf("%s", input);

    }while(strlen(input) > 30);

    strcpy(items[index].name, input);

    do
    {
        printf("\nBitte geben Sie eine Nummer fuer den Eintrag ein: ");
        scanf("%s", input);

    }while(strlen(input) > 30);

    strcpy(items[index].nummer, input);

    printf("\nBitte geben Sie ein Jahr fuer den Eintrag ein: ");
    scanf("%ld", &items[index].jahr);

}


void sortItems(struct Inventar_s items[], long len, int which)
{
    long pos = 0;
    do
    {
        pos = 0;
        do
        {
            if((which == 1 && strcmp(items[pos].name, items[pos+1].name) > 0) ||
                (which == 2 && strcmp(items[pos].nummer, items[pos+1].nummer) > 0) ||
                (which == 3 && items[pos].jahr > items[pos+1].jahr))

            {
                char temp[30];

                strcpy(temp, items[pos+1].name);
                strcpy(items[pos+1].name, items[pos].name);
                strcpy(items[pos].name, temp);

                strcpy(temp, items[pos+1].nummer);
                strcpy(items[pos+1].nummer, items[pos].nummer);
                strcpy(items[pos].nummer, temp);

                long ltemp = items[pos+1].jahr;
                items[pos+1].jahr = items[pos].jahr;
                items[pos].jahr = ltemp;

            }

        }while(++pos < len - 1);
    }while(--len > 0);
}



