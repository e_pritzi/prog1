#include <stdio.h>
#include <stdlib.h>

//Funktionsdeklarationen
long wordlength(char text[]);
long others(char text[]);

int main()
{
    char input[100];
    long i = 0, len = 0;
    long length[10] = { 0 };

    printf("Bitte geben Sie einen Text ein: ");

    //Array auff�llen und mit String Ende Character versehen
    while((input[i++] = getchar()) != '\n');
    input[i-1] = '\0';

    //BUchstaben z�hlen
    printf("wordlength: %ld\n", wordlength(input));
    printf("others: %ld\n", others(input));

    //W�rter z�hlen
    i = 0;

    do{
        if((input[i] == ' ' || input[i] == '\0') && input[i-1] != ' ')
            len++;
        i++;
    }while( input[i-1] != '\0');


    printf("Gefundene Woerter: %ld\n", len);

    //Buchstabenverteilung angeben
    char *ptr = input;
    do
    {
        long lentemp = wordlength(ptr);

        if(lentemp < 11 && lentemp >= 1)
            length[lentemp - 1]++;

        ptr += lentemp + 1;

    }while(*(ptr-1) != '\0');

    for(i = 0; i < 10; i++)
        printf("%2ld: %2ld\n", i+1, length[i]);

    return 0;
}


//Z�hlt Buchstaben (a-z) (A-Z)
long wordlength(char text[])
{
    long length = 0;

    while(((*text >= 'a' && *text <= 'z') || (*text >= 'A' && *text <= 'Z')) && *text != 0)
    {
        //Umlaute filtern
        if(! (*text == 'e' && (*(text - 1) == 'a' || *(text - 1) == 'o' || *(text - 1) == 'u')))
            length++;
        text++;
    }

    return length;

    /*
    //f�r Experten

    if(*(text + length) == '\0')
        return NULL;
    else
        return text + length;
    */
}

//Z�hlt andere Zeichen
long others(char text[])
{
    long length = 0;

    while(!((*text >= 'a' && *text <= 'z') || (*text >= 'A' && *text <= 'Z')) && *text != '\0')//besser mit isdigit()
    {
        length++;
        text++;
    }

    return length;

    /*
    //f�r Experten

    if(*(text + length) == '\0')
        return NULL;
    else
        return text + length;
    */
}
