#include <stdio.h>
#include <stdlib.h>
#include <math.h>

//Structdeklarationen
typedef struct VECTOR_s
{
    double x;
    double y;
    double z;
} VECTOR_t;

typedef struct CYLINDER_s
{
    double r;
    double phi;
    double z;
} CYLINDER_t;

//Funktionsdeklarationen
void printVector(char name[], VECTOR_t vec);
VECTOR_t readVector();
double absVector(VECTOR_t vec);
VECTOR_t normVector(VECTOR_t vec);
VECTOR_t multVectorEx(VECTOR_t a, VECTOR_t b);
double multVectorIn(VECTOR_t a, VECTOR_t b);
CYLINDER_t transformCylinder(VECTOR_t a);
void readFromFile(VECTOR_t *vecA, VECTOR_t *vecB, VECTOR_t *vecC);
void writeToFile(VECTOR_t vecA, VECTOR_t vecB, VECTOR_t vecC);


//MAIN
int main()
{
    VECTOR_t vecA, vecB, vecC;

    long choice;

    do
    {
        //print Menue
        printf("\n1. Vektoren ausgeben");
        printf("\n2. Vektor A einlesen");
        printf("\n3. Vektor B einlesen");
        printf("\n4. Betrag ausgeben");
        printf("\n5. Norm");
        printf("\n6. Vektorprodukt");
        printf("\n7. Inneres Produkt");
        printf("\n8. Zylindertransformation");
        printf("\n9. In Datei schreiben");
        printf("\n10. Von Datei lesen");
        printf("\n11. Ende\n");

        scanf("%ld", &choice);

        //choose what to do
        switch(choice)
        {
            case 1:
                printVector("A", vecA);
                printVector("B", vecB);
                printVector("C", vecC);
                break;

            case 2:
                vecA = readVector();
                break;

            case 3:
                vecB = readVector();
                break;

            case 4:
                printf("\n|A| = %lf", absVector(vecA));
                printf("\n|B| = %lf", absVector(vecB));
                printf("\n|C| = %lf", absVector(vecC));
                break;

            case 5:
                normVector(vecA);
                break;

            case 6:
                multVectorEx(vecA, vecB);
                break;

            case 7:
                multVectorIn(vecA, vecB);
                break;

            case 8:
                transformCylinder(vecA);
                break;

            case 9:
                writeToFile(vecA, vecB, vecC);
                break;

            case 10:
                readFromFile(&vecA, &vecB, &vecC);
                break;

            case 11:
                break;

            default:
                printf("\n\nUngueltige Eingabe!!\n");
                break;
        }

    }while(choice != 11);


    return 0;
}


//print 1 vector
void printVector(char name[], VECTOR_t vec)
{
    printf("\n\t%lf\n", vec.x);
    printf("%s = \t%lf\n", name, vec.y);
    printf("\t%lf\n", vec.z);

}

//Add a new vector
VECTOR_t readVector()
{
    VECTOR_t tempVec;

    printf("\nVektor eingeben: \n\tx = ");
    scanf("%lf", &tempVec.x);

    printf("\ty = ");
    scanf("%lf", &tempVec.y);

    printf("\tz = ");
    scanf("%lf", &tempVec.z);

    return tempVec;
}

//calc absolute value
double absVector(VECTOR_t vec)
{
    return sqrt(vec.x * vec.x + vec.y * vec.y + vec.z * vec.z);
}

//norm vector
VECTOR_t normVector(VECTOR_t vec)
{
    double abs = absVector(vec);

    //norm means each element devided by the absolute value
    vec.x /= abs;
    vec.y /= abs;
    vec.z /= abs;

    printVector("||vec||", vec);

    return vec;
}


//vector product
VECTOR_t multVectorEx(VECTOR_t a, VECTOR_t b)
{
    VECTOR_t solution;

    solution.x = a.y*b.z-a.z*b.y;
    solution.y = a.z*b.x-a.x*b.z;
    solution.z = a.x*b.y-a.y*b.x;

    printVector("a x b", solution);

    return solution;
}

//in product
double multVectorIn(VECTOR_t a, VECTOR_t b)
{
    double solution;

    solution = a.x*b.x + a.y*b.y + a.z*b.z;

    printf("a . b = %lf\n", solution);

    return solution;
}


CYLINDER_t transformCylinder(VECTOR_t a)
{
    CYLINDER_t cylinder;

    cylinder.r = sqrt(a.x*a.x+a.y*a.y);
    cylinder.phi = atan2(a.y, a.x);
    cylinder.z = a.z;

    printf("R = \t %lf", cylinder.r);
    printf("phi = \t %lf", cylinder.phi);
    printf("z = \t %lf", cylinder.z);

    return cylinder;
}

void writeToFile(VECTOR_t vecA, VECTOR_t vecB, VECTOR_t vecC)
{
    char path[30];
    FILE *myFile;

    printf("\nBitte geben Sie einen Dateinamen ein: ");
    scanf("%s", path);

    myFile = fopen(path, "w+");

    if(myFile != NULL)
    {
        //Start Pointer, Laenge eines Elements, Anzahl der Elemente, Stream
        fwrite(&vecA, sizeof(vecA), 1, myFile);
        fwrite(&vecB, sizeof(vecB), 1, myFile);
        fwrite(&vecC, sizeof(vecC), 1, myFile);

        /*
        fprintf(myFile, "%lf%lf%lf", vecA.x, vecA.y, vecA.z);
        fprintf(myFile, "%lf%lf%lf", vecB.x, vecB.y, vecB.z);
        fprintf(myFile, "%lf%lf%lf", vecC.x, vecC.y, vecC.z);*/

        fclose(myFile);

        printf("\nDer Schreibvorgang war erfolgreich!");
    }



}

void readFromFile(VECTOR_t *vecA, VECTOR_t *vecB, VECTOR_t *vecC)
{
    char path[30];
    FILE *myFile;

    printf("\nBitte geben Sie einen Dateinamen ein: ");
    scanf("%s", path);

    myFile = fopen(path, "r");

    if(myFile != NULL)
    {
        if( fread(vecA, sizeof(*vecA), 1, myFile) ||
            fread(vecB, sizeof(*vecB), 1, myFile) ||
            fread(vecC, sizeof(*vecC), 1, myFile))
        {
            printf("\nDer Lesevorgang war erfolgreich!");
        }
        else
        {
            printf("\nFehler beim Lesen der Datei!");
        }

        /*
        fscanf(myFile, "%lf%lf%lf", &vecA->x, &vecA->y, &vecA->z);
        fscanf(myFile, "%lf%lf%lf", &vecB->x, &vecB->y, &vecB->z);
        fscanf(myFile, "%lf%lf%lf", &vecC->x, &vecC->y, &vecC->z);*/

        fclose(myFile);
    }
    else
    {
        //Nullpointer -> Fehler
        printf("\nDie Datei existiert nicht!");
    }
}
