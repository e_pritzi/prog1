#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

//Funktionsdeklarationen
void search(char filename[], char *cmp, long ignore);

int main(int argc, char *argv[])
{
    long index = 1;
    long ignore = 0;
    char cmp[1000];

    if(!strcmp(argv[1], "-i"))
    {
        index = 2;
        ignore = 1;
    }

    if(argc < 3 + ignore)
    {
        return 1;
    }

    strcpy(cmp, argv[index++]);

    //durch alle Elemente loopen
    do
    {
        search(argv[index++], cmp, ignore);

    }while(index < argc);

    /*
    printf("Bitte geben Sie ein Zeichen ein: ");
    char c;
    scanf("%c", &c);

    printf("Bitte geben Sie einen Pfad ein: ");
    char path[30];
    scanf("%s", path);

    search(path, c);
    */

    return 0;
}

void search(char filename[], char *cmp, long ignore)
{
    FILE * myFile;
    myFile = fopen(filename, "r+");

    //Fehler beim Oeffnen der Datei
    if(myFile == NULL)
    {
        fprintf(stderr, "\nDie Datei ist nicht verfuegbar!\n");
        return;
    }

    char input;
    long lineNum = 1;
    long pos = 0;

    printf("\n");

    while((input = fgetc(myFile)) != EOF)
    {
        if(input == '\n')
        {
            lineNum++;
            pos = 0;
        }

        else if((ignore && toupper(input) == toupper(cmp[pos])) ||
                (!ignore && input == cmp[pos]))
        {
            pos++;

            if(cmp[pos] == '\0')
            {
                pos = 0;
                printf("%s:", filename);
                printf("\t%ld\n", lineNum);
            }
        }
        else
        {
            pos = 0;
        }
    }

    fclose(myFile);

}
