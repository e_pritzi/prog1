#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

//Funktionsdeklarationen
void count(char filename[], long *chars, long *words, long *lines, long *sentences);

int main(int argc, char *argv[])
{
    long chars = 0, words = 0, lines = 0, sentences = 0;
    long gesChars = 0, gesWords = 0, gesLines = 0, gesSentences = 0;
    short sum = 0;

    //kein Argument uebergeben
    if(argc < 2)
        return 1;

    long i = 1;

    //-g
    if( !strcmp(argv[i], "-g") )
    {
        //erster Eintrag ist -g
        i++;
        sum = 1;
    }

    for(; i < argc; i++)
    {

        //fuer jede Datei wird die Funktion aufgerufen
        count(argv[i], &chars, &words, &lines, &sentences);

        //nur Ausgeben wenn kein -g eingegeben wurde
        if(!sum)
        {
            printf("\n%s:", argv[i]);
            printf("\nchars : %ld\nwords: %ld\nlines: %ld\nsentences: %ld\n", chars, words, lines, sentences);
        }

        //zu den gesamten Werten addieren
        gesChars += chars;
        gesLines += lines;
        gesWords += words;
        gesSentences += sentences;
    }

    printf("\nGesamt");
    printf("\nchars : %ld\nwords: %ld\nlines: %ld\nsentences: %ld\n", gesChars, gesWords, gesLines, gesSentences);

    return 0;
}


//Zaehlt die Zeichenanzahl
void count(char filename[], long *chars, long *words, long *lines, long *sentences)
{
    FILE * myFile;

    //bei 0 starten
    char c;
    *chars = 0;
    *words = 0;
    *lines = 0;
    *sentences = 0;
    short inWort = 0; //Hilfsvariable fuer folgende whitespaces
    short sentenceStarted = 0;

    if( (myFile = fopen(filename, "r+")) == NULL)
    {
        //Fehler beim Oeffnen der Datei
        fprintf(stderr, "\nDie Datei konnte nicht geoeffnet werden! ");
        return;
    }


    do
    {
        c = fgetc(myFile);

        if( isspace(c) )
        {
            //whitespace gefunden
            if(inWort)
            {
                (*words)++;
                inWort = 0; //jetzt befinde ich mich bei einem whitespace
            }
        }
        else if(isalpha(c))
        {
            //Zeichen gefunden
            (*chars)++;
            inWort = 1;

            //grossbuchstabe bedeutet satzanfang
            if(isupper(c))
                sentenceStarted = 1;

        }
        else if( c == '.' && sentenceStarted)
        {
            //Punkt bedeutet Satzende
            sentenceStarted = 0;
            (*sentences)++;
        }

        if(c == '\n')
        {
            //Zeilenumbruch
            (*lines)++;
        }

    }while(c != EOF);

    (*lines)++; //auch das EOF steht fuer ein Zeilenende

    fclose(myFile);
}
