#include <stdio.h>
#include "transistor.h"
#include <string.h>

void initializeTransistor(Transistor_t *transistor)
{
    static long id = 0;
    transistor->id = id;
    strcpy(transistor->type, "");
    transistor->characteristics.L = 0;
    transistor->characteristics.W = 0;
    transistor->characteristics.vht = 0;
    transistor->characteristics.tox = 0;
}

void initializeAllTransistors(Transistor_t transistor[], long len)
{
    long i;
    for(i = 0; i < len; i++)
    {
        initializeTransistor(&transistor[i]);
    }
}

void printTransistor(Transistor_t transistor)
{
    printf("Type: \t%s\n", transistor.type);
    printf("id: \t%ld\n", transistor.id);
    printf("Vth: \t%gV\n", transistor.characteristics.vht);
    printf("WxL: \t%1.1lf x %1.1lf um\n", transistor.characteristics.W, transistor.characteristics.L);
    printf("tox: \t%1.1lf\n\n", transistor.characteristics.tox);
}

void printAllTransistors(Transistor_t transistor[], long len)
{
    long i = 0;
    for(i = 0; i < len; i++)
    {
        printTransistor(transistor[i]);
    }
}

void editTransistor(Transistor_t transistor[], long len, long id)
{
    //id suchen
    long i;
    for(i = 0; i < len; i++)
    {
        if(transistor[i].id == id)
        {
            //Eintrag gefunden
            printTransistor(transistor[i]);

            //clear buffer
            while(getchar() != '\n');
            printf("\n\nWollen Sie den Type aendern? (y/n) ");
            if(getchar() == 'y')
            {
                //evtl clear buffer
                printf("Eingabe: ");
                scanf("%s", transistor[i].type);
            }

            //clear buffer
            while(getchar() != '\n');
            printf("\n\nWollen Sie die ID aendern? (y/n) ");
            if(getchar() == 'y')
            {
                //evtl clear buffer
                printf("Eingabe: ");
                scanf("%ld", &transistor[i].id);
            }

            //clear buffer
            while(getchar() != '\n');
            printf("\n\nWollen Sie L aendern? (y/n) ");
            if(getchar() == 'y')
            {
                //evtl clear buffer
                printf("Eingabe: ");
                scanf("%lf", &transistor[i].characteristics.L);
            }

            //clear buffer
            while(getchar() != '\n');
            printf("\n\nWollen Sie W aendern? (y/n) ");
            if(getchar() == 'y')
            {
                //evtl clear buffer
                printf("Eingabe: ");
                scanf("%lf", &transistor[i].characteristics.W);
            }

            //clear buffer
            while(getchar() != '\n');
            printf("\n\nWollen Sie VTH aendern? (y/n) ");
            if(getchar() == 'y')
            {
                //evtl clear buffer
                printf("Eingabe: ");
                scanf("%lf", &transistor[i].characteristics.vht);
            }

            //clear buffer
            while(getchar() != '\n');
            printf("\n\nWollen Sie Tox aendern? (y/n) ");
            if(getchar() == 'y')
            {
                //evtl clear buffer
                printf("Eingabe: ");
                scanf("%lf", &transistor[i].characteristics.tox);
            }

            return;
        }
    }

    //kein Eintrag gefunden
    for(i = 0; i < len; i++)
    {
        if(transistor[i].id == 0)
        {
            printf("\nEs wird ein neuer Eintrag hinzugefuegt!\nBitte geben Sie die Daten ein:\n");
            printf("type: ");
            scanf("%s", transistor[i].type);

            printf("ID: ");
            scanf("%ld", &transistor[i].id);

            printf("L: ");
            scanf("%lf", &transistor[i].characteristics.L);

            printf("W: ");
            scanf("%lf", &transistor[i].characteristics.W);

            printf("TOX: ");
            scanf("%lf", &transistor[i].characteristics.tox);

            printf("VTH: ");
            scanf("%lf", &transistor[i].characteristics.vht);

            return;
        }
    }

    //kein freier Eintrag
    printf("\nEs gibt leider keinen freien Eintrag! \n\n");
    return;
}

void writeBinaryTransistorData(Transistor_t transistor[], long len, char *filename)
{
    FILE *myFile;
    myFile = fopen(filename, "w+");
    if(myFile != NULL)
    {
        fwrite(transistor, sizeof(transistor[0]), len, myFile);
        fclose(myFile);
    }
    else
    {
        printf("\nFehler beim Oeffnen der Datei!\n");
    }

}


void readBinaryTransistorData(Transistor_t transistor[], long len, char *filename)
{
    FILE *myFile;
    myFile = fopen(filename, "r+");
    if(myFile != NULL)
    {
        fread(transistor, sizeof(transistor[0]), len, myFile);
        fclose(myFile);
    }
    else
    {
        printf("\nFehler beim Oeffnen der Datei!\n");
    }

}


void sortItems(Transistor_t transistor[], long len)
{
    long validItems = 0;
    long i = 0;
    for(i = 0; i < len; i++)
    {
        if(transistor[i].characteristics.L > 0 && transistor[i].characteristics.W > 0 && transistor[i].characteristics.tox > 0)
        {
            //guelitger Eintrag
            if(validItems < i)
            {
                Transistor_t temp = transistor[validItems];
                transistor[validItems] = transistor[i];
                transistor[i] = temp;
            }

            validItems++;

        }
    }
}

long compL(Transistor_t *a, Transistor_t *b)
{
    if(a->characteristics.L > b->characteristics.L)
        return 1;
    else
        return 0;
}

long compW(Transistor_t *a, Transistor_t *b)
{
    if(a->characteristics.W > b->characteristics.W)
        return 1;
    else
        return 0;
}

void sortList(Transistor_t transistor[], long len, long (*compare) (Transistor_t *a, Transistor_t *b))
{
    long i, j;

    //Bubble Sort
    for(i = len -1 ; i > 0; i--)
    {
        for(j = 0; j < i; j++)
        {
            if((*compare)(&transistor[j], &transistor[j+1]))
            {
                //dann tauschen
                Transistor_t temp = transistor[j];
                transistor[j] = transistor[j+1];
                transistor[j+1] = temp;
            }
        }
    }
}
