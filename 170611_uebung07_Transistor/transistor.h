#ifndef TRANSISTOR_H_INCLUDED
#define TRANSISTOR_H_INCLUDED
#define NMAX 30

//Structs
typedef struct Data_s
{
    double vht; //threshold voltage
    double L,W; //gate length, gate width
    double tox; // oxide thickness

}Data_t;


typedef struct Transistor_s
{
    char type[30]; //type of transistor
    long id; //unique number
    Data_t characteristics; //device parameter

}Transistor_t;

//Funktionen
void initializeTransistor(Transistor_t *transistor);
void initializeAllTransistors(Transistor_t transistor[], long len);
void printTransistor(Transistor_t transistor);
void printAllTransistors(Transistor_t transistor[], long len);
void editTransistor(Transistor_t transistor[], long len, long id);
void writeBinaryTransistorData(Transistor_t transistor[], long len, char *filename);
void readBinaryTransistorData(Transistor_t transistor[], long len, char *filename);
void sortItems(Transistor_t transistor[], long len);

long compL(Transistor_t *a, Transistor_t *b);
long compW(Transistor_t *a, Transistor_t *b);
void sortList(Transistor_t transistor[], long len, long (*compare) (Transistor_t *a, Transistor_t *b));

#endif // TRANSISTOR_H_INCLUDED
