#include <stdio.h>
#include <stdlib.h>
#include "transistor.h"

#define COUNTTRANS 5

int main()
{
    Transistor_t myTransistors[COUNTTRANS];

    initializeAllTransistors(myTransistors, COUNTTRANS);

    long choice;
    do
    {
        printf("\nBitte waehlen Sie aus: \n");
        printf("1 - Einen Transistor zuruecksetzen \n");
        printf("2 - Die gesamte Datenbank zuruecksetzen \n");
        printf("3 - Einen Transistor ausgeben\n");
        printf("4 - Alle Transistoren ausgeben\n");
        printf("5 - Datensatz in Datei schreiben\n");
        printf("6 - Datensatz von Datei lesen\n");
        printf("7 - Edit\n");
        printf("8 - Nach L sortieren\n");
        printf("9 - Nach W sortieren\n");
        printf("10 - Ende\n");

        scanf("%ld", &choice);

        long index;
        char path[30];

        switch(choice)
        {
            case 1:
                //einen zuruecksetzen

                printf("\nWelchen moechten Sie gerne zuruecksetzen? (Index): ");
                scanf("%ld", &index);
                if(index < COUNTTRANS)
                    initializeTransistor(&myTransistors[index]);
                break;

            case 2:
                //alle zuruecksetzen
                initializeAllTransistors(myTransistors, COUNTTRANS);
                break;

            case 3:
                //einen Transistor ausgeben
                printf("\nWelchen moechten Sie gerne ausgeben? (Index): ");
                scanf("%ld", &index);
                if(index < COUNTTRANS)
                    printTransistor(myTransistors[index]);
                break;

            case 4:
                //alle Transistoren ausgeben
                printAllTransistors(myTransistors, COUNTTRANS);
                break;

            case 5:
                //schreiben
                printf("\nBitte geben Sie einen Pfad ein: ");
                scanf("%s", path);
                writeBinaryTransistorData(myTransistors, COUNTTRANS, path);
                break;

            case 6:
                //lesen
                printf("\nBitte geben Sie einen Pfad ein: ");
                scanf("%s", path);
                readBinaryTransistorData(myTransistors, COUNTTRANS, path);
                break;

            case 7:
                printf("\nWelchen moechten Sie gerne aendern? (ID): ");
                scanf("%ld", &index);
                editTransistor(myTransistors, COUNTTRANS, index);
                break;

            case 8:
                //nach L sortieren
                sortList(myTransistors, COUNTTRANS, &compL);
                break;

            case 9:
                //nach W sortieren
                sortList(myTransistors, COUNTTRANS, &compW);
                break;

            default:
                break;
        }

        sortItems(myTransistors, COUNTTRANS);

    }while(choice != 10);
    return 0;
}
