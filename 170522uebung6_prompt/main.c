#include <stdio.h>
#include <stdlib.h>

long prompt();

int main()
{

    FILE * myFile;
    myFile = fopen("test.txt", "w+");

    fprintf( myFile, "Der Text kommt zu Beginn" );
    fseek( myFile, 4, SEEK_SET);
    fprintf( myFile, "Code");
    fseek(myFile, 0, SEEK_END);
    fprintf(myFile, "\nund das kommt ans Ende");

    fclose(myFile);

    return 0;
}


long prompt()
{
    char v_readChar;
    printf("\n\n[Enter druecken]\n\n");

    scanf("%c", &v_readChar);
    while(v_readChar != '\n' && getchar() != '\n' );

    switch (v_readChar)
    {
    case '\n':
        return 0;
        break;
    case 'q':
        return 1;
        break;
    case 'n':
        return 2;
        break;
    default:
        printf("\nEnter...\tAusgabe fortsetzen");
        printf("\nh...\tHilfe");
        printf("\nq...\tBeenden");
        printf("\nn...\tZur naechsten Datei/Beenden");
        return -1;
        break;
    }
}
