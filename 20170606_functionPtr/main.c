#include <stdio.h>
#include <stdlib.h>

//erste Funktion
long add(long a, long b){
    return a+b;
}

//zweite Funktion
long times(long a, long b){
    return a*b;
}

//Funktion die den FktPointer verwendet --> Funktion auswertet und ausgibt
void printSol(long a, long b, long (*funcPtr) (long, long)){
    long solution = (*funcPtr) (a, b); //berechnen
    printf("%ld", solution); //ausgeben
}


int main()
{
    //FktPtr deklarieren
    long (*funcPtr)(long,long);

    funcPtr = &add; // auf die add Funktion verweisen
    printSol(2,5, funcPtr); //Funktion mit FktPtr aufrufen
    printf("\n\n");

    funcPtr = &times; //auf die times Fkt verweisen
    printSol(2,5,funcPtr); // und mit diesem Verweis ausfuehren
    printf("\n\n");

    return 0;
}
